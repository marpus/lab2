package com.example.testy;

import android.content.Context;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;

public class ListActivity extends AppCompatActivity {

    private ListView list;

    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        list = (ListView) findViewById(R.id.listViev);
        String student[] = {"Student1", "Student2", "Student3", "Student4", "Student5"};
        
        ArrayList<String> carL = new ArrayList<String>();
        carL.addAll(Arrays.asList(student));

        adapter = new ArrayAdapter<String>(this, R.layout.row, student);
        list.setAdapter((ListAdapter) adapter);

    }
}
