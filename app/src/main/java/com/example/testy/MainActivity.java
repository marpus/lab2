package com.example.testy;

import android.content.Intent;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private ListView list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickTxt(View view, ListAdapter adapter) {
        Intent k = new Intent(MainActivity.this, ListActivity.class);
        list.setAdapter(adapter);
    }
}
